<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ["T-Shirt", "Jeans", "Jacket", "Pants", "Socks"];
        foreach ($categories as $item) {
            $category = new Category();
            $category->name = $item;
            $category->save();
        }
    }
}
