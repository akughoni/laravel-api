<?php

use App\Category;
use App\Http\Resources\Product as ResourcesProduct;
use App\Http\Resources\ProductCollection;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get("/category/{id?}", function ($id = null) {
    if (is_null($id)) {
        $data = Category::with("products")->get();
    } else {
        $data = Category::where("id", $id)->with("products")->first();
    }
    if (is_null($data)) {
        return response()->json([
            "message" => "data not found"
        ], 404);
    }

    return response()->json($data);
});

Route::get("/product/{id?}", function ($id = null) {
    if (is_null($id)) {
        $data = Product::with('category')->get();
        // return response()->json($data, 200);
        return new ProductCollection($data);
    }

    $data = Product::where("id", $id)->with("category")->first();
    return new ResourcesProduct($data);
    // return response()->json($data, 200);
});


Route::post("/product/add", function (Request $req) {
    $rules = [
        "title" => "required",
        "description" => "required",
        "prices" => "required",
        "category_id" => "required"
    ];

    $valid = Validator::make($req->all(), $rules);

    if ($valid->fails()) {
        return response()->json([
            "status" => "bad request"
            ]);
    }

    $data = Product::create([
        "title" => $req->title,
        "description" => $req->description,
        "prices" => $req->prices,
        "category_id" => $req->category_id
    ]);

    return response()->json([
        "status" => "success",
        "data" => $data
    ]);
});

Route::put("/product/update/{id}", function (Request $req, $id) {
    $data = Product::find($id);
    $data->title = $req->title;
    $data->description = $req->description;
    $data->prices = $req->prices;
    $data->category_id = $req->category_id;
    $data->save();

    return response()->json([
        "status" => "success",
        "data" => $data
    ]);
});

Route::delete('/product/delete/{id}', function ($id) {
    $data = Product::destroy($id);

    return response()->json($data);
});
